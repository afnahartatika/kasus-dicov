package com.ddp2.lab10kamis;

import java.util.ArrayList;

import com.ddp2.lab10kamis.model.KasusDicov;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {
    @GetMapping("/")
    public String index(Model model) {
        // TODO: Lengkapi dengan mengembalikan atribut nama ke index.html
        model.addAttribute("nama", "Afna");
        return "index";
    }

    @GetMapping("/isi-kasus")
    public String isiKasus(Model model) {
        // TODO:
        // Lengkapi dengan mengembalikan atribut kasusDicov, listJenisKelamin, dan listStatus
        // ke form-isi-kasus.html     
        model.addAttribute("kasusDicov", new KasusDicov()); 

        ArrayList<String> listJenisKelamin = new ArrayList<String>();
        listJenisKelamin.add("Laki-laki");
        listJenisKelamin.add("Perempuan");
        model.addAttribute("listJenisKelamin", listJenisKelamin);

        ArrayList<String> listStatus = new ArrayList<String>();
        listStatus.add("Positif");
        listStatus.add("Negatif");
        model.addAttribute("listStatus", listStatus);

        return "form-isi-kasus";
    }

    @PostMapping("/isi-kasus")
    public String submitIsiKasus(@ModelAttribute KasusDicov kasusDicov) {
        // TODO: Tambahkan kasusDicov ke datfar seluruh kasus
        KasusDicov.tambahKasus(kasusDicov);
        return "hasil-isi-kasus";
    }

    @GetMapping("/lihat-semua-kasus")
    public String lihatSemuaKasus(Model model) {
        // TODO: Lengkapi dengan atribut yang dibutuhkan untuk informasi tabel
        model.addAttribute("listKasus", KasusDicov.getListKasusDicov());
        model.addAttribute("genderList", KasusDicov.getJumlahKasusBerdasarkanStatus());
        model.addAttribute("statusList", KasusDicov.getJumlahKasusBerdasarkanStatus());
        model.addAttribute("totalKasus", KasusDicov.getJumlahKasus());
        return "lihat-semua-kasus";
    }
}