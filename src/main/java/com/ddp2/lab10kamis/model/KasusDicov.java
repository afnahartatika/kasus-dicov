package com.ddp2.lab10kamis.model;

import java.util.ArrayList;

public class KasusDicov {
    private static ArrayList<KasusDicov> listKasusDicov = new ArrayList<KasusDicov>();
    // TODO: Lengkapi dengan atribut yang diperlukan class 
    private String nama;
    private long umur;
    private String jenisKelamin;
    private String alamat;
    private String status;

    // TODO: Lengkapi dengan setter dan getter
    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getUmur() {
        return this.umur;
    }

    public void setUmur(long umur) {
        this.umur = umur;
    }

    public String getJenisKelamin() {
        return this.jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // TODO: Lengkapi fungsi berikut untuk menambahkan kasus baru ke listKasusDicov
    public static void tambahKasus(KasusDicov kasus) {
        listKasusDicov.add(kasus);
    }

    // TODO: Ubah fungsi berikut untuk mengembalikan list seluruh kasus Dicov
    public static ArrayList<KasusDicov> getListKasusDicov() {
        return listKasusDicov;
    }

    // TODO: Lengkapi fungsi berikut. Fungsi ini mengembalikan array dengan length 2
    // dimana arr[0] adalah jumlah kasus dengan jenis kelamin laki-laki dan
    // arr[1] adalah jumlah kasus dengan jenis kelamin perempuan
    public static long[] getJumlahKasusBerdasarkanJenisKelamin() {
        long[] jenisKelaminArr = { 0, 0 };
        for (KasusDicov kasus : listKasusDicov) {
            if (kasus.jenisKelamin.equals("Laki-laki")) {
                jenisKelaminArr[0]++;
            } else {
                jenisKelaminArr[1]++;
            }
        }
        return jenisKelaminArr;
    }

    // TODO: Lengkapi fungsi berikut. Fungsi ini mengembalikan array dengan length 2
    // dimana arr[0] adalah jumlah kasus dengan status positif dan
    // arr[1] adalah jumlah kasus dengan status negatif
    public static long[] getJumlahKasusBerdasarkanStatus() {
        long[] statusArr = { 0, 0 };
        for (KasusDicov kasus : listKasusDicov) {
            if (kasus.status.equals("Positif")) {
                statusArr[0]++;
            } else {
                statusArr[1]++;
            }
        }
        return statusArr;
    }

    // TODO: Ubah fungsi berikut untuk mengembalikan jumlah seluruh kasus Dicov
    public static long getJumlahKasus() {
        return listKasusDicov.size();
    }
}